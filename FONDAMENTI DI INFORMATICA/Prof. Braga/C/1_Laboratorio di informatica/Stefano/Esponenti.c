#include <stdio.h>

int main()
{
	int b=0, e=0, eprov=1;
	long long int ris=0;
	printf("----POTENZE DI UN NUMERO----\n");
	printf("Inserisci una base ed un esponente (intero positivo): ");
	scanf("%d%d", &b, &e);
	ris=b;
	eprov=1;
	if(e==0)
		printf("%d^0 = 1\n", b);
	if(e==1){
		printf("%d^0 = 1\n", b);
		printf("%d^1 = %d\n", b, b);
	}
	if(e>1){
		printf("%d^0 = 1\n", b);
		printf("%d^1 = %d\n", b, b);
		while(e>1){	
			ris=ris*b;
			e--;
			eprov++;
			printf("%d^%d = %lld\n", b, eprov, ris);
		}
	}
	if(e<0)
		printf("L'esponente e' minore di 0");
	
	return 0;
}
