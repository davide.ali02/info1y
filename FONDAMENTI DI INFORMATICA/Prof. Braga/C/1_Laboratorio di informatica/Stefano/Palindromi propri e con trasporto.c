#include <stdio.h>
#include <string.h>
#include <math.h>

	int palindromi(char str[]);

int main(){
	
	char c=0, p=0, pt=0;
	char str[100] = "scrivere";
	
	printf("--Palindromi propri o con trasporto--\n");
	printf("Scrivi una frase: ");
	
	scanf("%s", &str);
	for(; strlen(str)>1; ){
		if(strlen(str)>2){
			if(palindromi(str)==1){
				printf("\"%s\" e' palindroma\n", str);
				p++;
			}
			else if(palindromi(str)==2){
				printf("\"%s\" e' palindroma con trasporto\n", str);
				pt++;
			}
		}
		scanf("%s", str);
	}
	
	printf("Hai digitato in totale %d parole palindrome e %d con trasporto\n", p, pt);
	
	return 0;
}

	int palindromi(char str[]){								//0==non palindromi,   1==palindromi,	2==palindromi con trasporto
		int i=0, j=0, cont=0;
		cont=(strlen(str)/2)-1;
		j=strlen(str)-1;
		for(i=0, j=strlen(str)-1; i<=cont; i++, j--){				//primo tentativo (si parte dalla prima lettera ovviamente)
			if(str[i]!=str[j]){
				break;
			}
			else if(i==cont && str[i]==str[j]){
				return 1;
			}
		}
		for(i=1, j=strlen(str)-1, cont+=1; i<=cont; i++, j--){		//si parte dalla seconda lettera
			if(str[i]!=str[j]){
				break;
			}
			else if(i==cont && str[i]==str[j]){
				return 2;
			}
		}
		for(i=0, j=strlen(str)-2; i<=cont; i++, j--){
			if(str[i]!=str[j]){
				break;
			}
			else if(i==cont && str[i]==str[j]){
				return 2;
			}
		}
		return 0;
	}
