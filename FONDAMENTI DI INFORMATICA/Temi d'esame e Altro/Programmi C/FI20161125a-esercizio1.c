/*
A.  Scrivere un frammento di codice (da inserire dove indicato nello scheletro sopra riportato) che memorizzi in v una sequenza di numeri interi positivi
    inseriti da tastiera; la sequenza termina non appena viene inserito un valore minore o uguale a zero, oppure al raggiungimento di 100 elementi positivi;
    la lunghezza della sequenza letta (ad eccezione dell’ultimo eventuale elemento non positivo) deve essere memorizzata nella variabile nv.

B.  Implementare la funzioni multipli in modo che (i) la chiamata nm = multipli(num,v,mult,nv) ritorni il numero di elementi in v che sono multipli di num,
    e (ii) che l’array mult alla fine dell’esecuzione della funzione contenga (a partire dalla prima posizione) solo gli elementi di v che sono multipli di num.
*/

#include <stdio.h>

int multipli(int num, int v[], int p[], int l);

int main() {

    int num, mult[100], v[100], nm, nv;

    printf("Inserisci num: ");
    scanf("%d",&num);

  /* Inserire qui il codice richiesto dalla DOMANDA A */

  nv = 0;
  do {
      printf("Inserisci numero: ");
      scanf("%d", &v[nv]);
      nv++;
  } while(v[nv-1]>0 && nv<100);

  nv--;


  // ***

    nm = multipli(num,v,mult,nv);

    int i=0;
    for (i=0; i<nm; i++)
        printf("%d ",mult[i]);

    printf("\n");

    return 0;
}

int multipli(int num, int v[], int p[], int l) {

    int i, k = 0;
    for(i=0; i<l; i++) {
        if(v[i]%num == 0) {
            p[k] = v[i];
            k++;
        }
    }

    return k;
}
