/*
Un modo molto semplice per criptare un messaggio di testo è quello di modificare ogni carattere contenuto nel messaggio applicando la seguente funzione:

C’ = (C+K)%255

dove C è il carattere originale del messaggio, C’ è il carattere criptato, K è un intero detto chiave che serve a criptare il messaggio, e
% è l’operatore matematico che fornisce il resto della divisione intera.

Scrivere un programma che legge da tastiera un intero K, apre il file di testo “messaggio.txt” e ne riscrive il contenuto nel file “codice.txt” dopo aver applicato
la funzione di criptazione descritta in precedenza, utilizzando la chiave K letta da tastiera.

Nota. Si ipotizzi che l’apertura dei due file di testo vada sempre a buon fine.

*/

#include <stdio.h>
#include <stdlib.h>

int main() {
    int k;
    char c;
    printf("Inserisci la chiave di criptazione: ");
    scanf("%d", &k);

    FILE *input, *output;
    input = fopen("messaggio.txt", "r");
    output = fopen("codice.txt", "w");

    while((c = getc(input)) != EOF) {
        putc((c+k)%255, output);
        printf("%d ", (c+k)%255); }

    fclose(output);
    fclose(input);

    return 0;
}
