/*
Input: una successione di numeri interi terminante con uno 0 (quando mette lo 0 la sequenza finisce)

Output: stampare a video il minimo e il massimo degli elementi inseriti
*/

// N.B: in questo caso creo un array con dimnesione 999, ma se l'utente inserisce più di 1000 numeri l'array non è abastanza capiente

#include <stdio.h>

int main()
{
    int sequenza[999];
    int i = 0, j = 0, max, min;

    do {
        printf("Inserisci il valore: ");
        scanf("%d%*c", &sequenza[i]);
        i++;
    } while(sequenza[i-1] != 0);

    min = sequenza[0];
    max = sequenza[0];

    for(j=0; j<i-1; j++) {
        printf("%d\n %d",sequenza[j], min );
        if(sequenza[j]<min)
            min = sequenza[j];
        if(sequenza[j]>max)
            max = sequenza[j];
    }

    if(i == 1) //se i==1 vuol dire che ho inserito solo lo 0, quindi non ho inserito nessun valore
        printf("Nessun numero inserito\n");
    else
        printf("Max: %d\nMin: %d\n", max, min);

    return 0;
}
